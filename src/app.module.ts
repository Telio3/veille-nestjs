import { Module } from '@nestjs/common';
import { EventsModule } from './socket/events.module';
import { UploadModule } from './upload/upload.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import configuration from './config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    UploadModule,
    EventsModule,
    UsersModule,
    AuthModule,
  ],
})
export class AppModule {}
