import { ApiProperty } from "@nestjs/swagger";

export class PostBodyUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  video: any;
}

export class PostResponseUploadDto {
  @ApiProperty({ type: 'string' })
  filename: string;
}
