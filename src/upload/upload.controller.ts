import {
  Controller,
  Get,
  Header,
  Headers,
  HttpStatus,
  Param,
  Post,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { randomUUID } from 'crypto';
import { diskStorage } from 'multer';
import { join } from 'path';
import { promisify } from 'util';
import { createReadStream, stat } from 'fs';
import { Response } from 'express';
import { ApiBody, ApiConsumes, ApiResponse, ApiTags } from '@nestjs/swagger';
import { PostBodyUploadDto, PostResponseUploadDto } from './dto/upload.dto';

@ApiTags('upload')
@Controller('upload')
export class UploadController {
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: PostBodyUploadDto })
  @ApiResponse({ type: PostResponseUploadDto })
  @Post()
  @UseInterceptors(
    FileInterceptor('video', {
      storage: diskStorage({
        destination: './upload',
        filename: (_, file, cb) => {
          cb(null, `${randomUUID()}.${file.mimetype.split('/')[1]}`);
        },
      }),
    }),
  )
  uploadFile(@UploadedFile() file: Express.Multer.File) {
    return { filename: file.filename.split('.')[0] };
  }

  @Get(':id')
  @Header('Content-Type', 'video/mp4')
  @Header('Accept-ranges', 'bytes')
  async getFile(
    @Res() res: Response,
    @Headers() headers,
    @Param('id') id: string,
  ) {
    const video = join(process.cwd(), `/upload/${id}.mp4`);

    const range = headers.range;
    if (!range) {
      createReadStream(video).pipe(res);
      return;
    }

    const parts = range.replace('bytes=', '').split('-');
    const videoStat = await promisify(stat)(video);
    const start = parseInt(parts[0], 10);
    const end = parts[1] ? parseInt(parts[1], 10) : videoStat.size - 1;

    res.set({
      'Content-Range': `bytes ${start}-${end}/${videoStat.size}`,
      'Content-Length': end - start + 1,
    });

    res.status(HttpStatus.PARTIAL_CONTENT);

    const readStreamfile = createReadStream(video, { start, end });
    readStreamfile.pipe(res);
  }
}
