import { Injectable } from '@nestjs/common';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  private users: User[] = [
    {
      id: 1,
      name: 'John',
      email: 'john@john.john',
      password: 'johnPass',
    },

    {
      id: 2,
      name: 'Gary',
      email: 'gary@gary.gary',
      password: 'garyPass',
    },
  ];

  findOneByEmail(email: string): Promise<User | undefined> {
    return Promise.resolve(this.users.find((user) => user.email === email));
  }

  findOneById(id: number): Promise<User | undefined> {
    return Promise.resolve(this.users.find((user) => user.id === id));
  }
}
