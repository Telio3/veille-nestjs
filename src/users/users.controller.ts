import {
  Controller,
  ForbiddenException,
  Get,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { JwtPayload } from '../auth/types';
import { UserDto } from './dto/user.dto';
import { UsersService } from './users.service';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiResponse({ type: UserDto })
  @UseGuards(JwtAuthGuard)
  @Get('me')
  async me(@Request() req: { user: JwtPayload }): Promise<UserDto> {
    const user = await this.usersService.findOneById(req.user.sub);

    if (!user) throw new ForbiddenException('Access Denied');

    delete user.password;

    return user;
  }
}
